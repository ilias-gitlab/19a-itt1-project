![Build Status](https://gitlab.com/EAL-ITT/19a-itt1-project/badges/master/pipeline.svg)


# 19A-ITT1-project

weekly plans, resources and other relevant stuff for the project in IT technology 1. semester autumn.

public website for students:

*  [gitlab pages](https://eal-itt.gitlab.io/19a-itt1-project/)