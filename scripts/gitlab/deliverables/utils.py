import sys

def get_token( filename='token.txt', out=sys.stdout ):
	out.write('using token path {}\n'.format(filename))
	with open(filename) as f:
		token=f.readline()
	out.write( "- read token with {} chars\n".format(len(token.strip())))
	return token.strip()

def get_filename_from_argv( index=1,  default_filename='groups.txt' ):
	if len(sys.argv) > index:
		return sys.argv[index]

	return default_filename
