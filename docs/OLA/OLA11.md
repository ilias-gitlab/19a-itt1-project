# Obligatory Learning Activity 11 (OLA11)

### Course 

1st Semester project

### Class 

OEAIT19EIA and OEAIT19EIB

### Topic

Interim presentation of Semester 1 project

## Information

For rules and regulations about OLA, see the semester description on itslearning, in the folder, ressources/general education documents.

## Exercise instructions

OLA11 is the interim presentation of semester 1 project.
The presentation is a report where students reflect on their contribution to the project as well as their overview of the project.

## Hand-in on wiseflow

The hand-in is a pdf report handed in on wiseflow. It will contain the following:

* A report describing your own contribution to the project done in phase 1 (Idea and research)
    * What was you main tasks? 
    * How did you structure your work? use examples
    * How did you communicate and resolve conflicts in the team? use examples
    * How did you document your work? use examples.

    * In appendix, include a graphical overview of the project phases including your agreed milestones for each project phase.

    We expect 3-4 pages + appendix. Note that this is an individual hand-in.

    **Use the file "19S Report and assignment document standard PDA V01.docx" attached to wiseflow as extra material**

Follow-up
============

After the hand-in period, the teachers will review the reports and notify you on wiseflow