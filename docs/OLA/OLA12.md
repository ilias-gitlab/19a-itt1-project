# Obligatory Learning Activity 12 (OLA12)

### Course 

1st Semester project

### Class 

OEAIT19EIA and OEAIT19EIB

### Topic

Project poster presentation

## Information

For rules and regulations about OLA, see the semester description on itslearning, in the folder, ressources/general education documents.

## Exercise instructions

OLA13 is the project presentation of semester 1 project.
The presentation is a poster presentation and will be held on 2020-01-06 - 13:00

Suggested presentation/poster content:

1. Presentation of project management
    * How did we perform project management. 
    * What worked and what did not work ?
    * How did we divide tasks between group members ? 
    * How did we evaluate tasks ?
    * How did we document tasks ?
    * If possible how will we improve project management in future projects

2. Presentation of results
    * What did we set out to accomplish ?
    * What did we accomplish ?
    * Prototype demo (if possible)
    * Other relevant results (survey, test results etc.)

3. Presentation of gained subject knowledge
    * What knowledge did we gain regarding: 
        * electronics
        * programming
        * embedded systems
        * networking     
        * project
    * If possible present your understanding of how the above course subjects correlate to each other 


## Hand-in on wiseflow

The hand-in is a group handin of the poster from the poster presentation. Hand in is on wiseflow.

Follow-up
============

After the hand-in period, the teachers will review the presentation and poster. Results are communicated on wiseflow