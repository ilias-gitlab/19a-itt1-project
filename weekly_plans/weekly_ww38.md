---
Week: 38
Content:  Idea and research
Material: See links in weekly plan
Initials: NISI/ILES
---

# Week 38 ITT1 Project - Idea and research

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* All groups have an agenda for teacher meeting
* All groups have completed Exercise 0, 1 and 2

### Learning goals

* How to perform a structured team meeting
* What S.M.A.R.T tasks are and how to use them
* Induction theory
* Contents of a project plan

## Deliverables

1. project plan draft

2. Mandatory weekly meetings with the teachers (this includes minutes of the meeting)
    * Agenda is:
        1. Status on project (ie. show closed tasks in gitlab)
        2. Next steps (ie. show next tasks in gitlab)
        3. Collaboration within the group (ie. any internal issues, fairness of workload, communication)
        4. Help needed or offered (ie. what help do you need and where do you feel you can contribute to the class)
        5. Any other business (AOB)

3. Minutes of meeting, from weekly teacher meeting, included in appropiate folder in gitlab project

4. Results from induction exercise in gitlab project

5. S.M.A.R.T tasks created

## Schedule Monday 2019-09-16

* 8:15 Introduction to the day, general Q/A session

    class representatives  
    1 representative and 1 substitute for each class  
    Agenda for teacher meetings  
    Attendance

* 9:00 Exercise work

    One half of 1. semester students go to E-lab and the other half stays in B2.05/B2.49    
 
    B2.05/B2.49: Exercise 0 and Exercise 1    
    E-lab: Exercise 2  

* 9:00 Study start test 2nd attempt, room A0.07+8+9

    For students that did not participate or passed the study start test in week 37

* 10:30 Switch between E-Lab and B2.05/B2.49

    If you still have work to do from Exercise 0 and 2 you can continue 12:30

* 12:30 Teacher meetings 

    All 1. semester groups and one or more members from 3. semester groups  

    Timeslot for your weekly 15 min. meeting with the teachers.  

    Remember to book a time and have an agenda prepared.  

    Add minutes of meeting in your gitlab project  

* 12:30 Continue exercise and task work

    Exercise 1: Define S.M.A.R.T tasks in gitlab

## Hands-on time

* Exercise 0: Project plan draft

     Make a project plan from this [template](https://eal-itt.gitlab.io/19a-itt1-project/project_plan_for_students.md) and upload it to your gitlab project  
     Include the results of the pre mortem exercise from ww37 in the project plan

* Exercise 1: Define S.M.A.R.T tasks in gitlab

     Define S-M.A.R.T tasks in [gitlabs issue board](https://docs.gitlab.com/ee/user/project/issues/)  

     Read about SMART tasks [here](https://gigaom.com/2008/05/05/how-to-create-smart-tasks/)

* Exercise 2: Induction

     All groups complete the [induction exercise](https://docs.google.com/presentation/d/1BI7LsdooHQr6zxEBPZtOJoyzo7Emnmx8yB6q-FuprHU/edit?usp=sharing)

## Comments

Workshop "How to survive in Denmark - and how to find a student job" for international students 13:45

\pagebreak