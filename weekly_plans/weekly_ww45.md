---
Week: 45
Content:  Development phase
Material: See links in weekly plan
Initials: NISI/ILES
---

# Week 45 ITT1 Project - Development

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* All groups are working on tasks   

### Learning goals

The student has knowledge about:

* User Study Methods 
* Customer feedback 
* Survey Management
* User Data Management

## Deliverables

1. Mandatory weekly meetings with the teachers (this includes minutes of the meeting)
    
    * Agenda is:
        1. Status on project (ie. show closed tasks in gitlab)
        2. Next steps (ie. show next tasks in gitlab)
        3. Collaboration within the group (ie. any internal issues, fairness of workload, communication)
        4. Help needed or offered (ie. what help do you need and where do you feel you can contribute to the class)
        5. Attendance
        6. Any other business (AOB)

2. Minutes of meeting, from weekly teacher meeting, included in appropiate folder in gitlab project

3. Each group member has a Gitlab tasks for the survey exercise

## Schedule Monday 2019-11-04

* 8:15 Introduction to the day, general Q/A session

* 8:45 Group morning meetings

    You will meet in your groups every morning and decide on who does what. This is part of the project management, and is mandatory.
    Ordinary agenda:
    1. (5 min) Round the table: What did I do, and what did I finish?
    2. (5-10 min) Review of tasks: Are they still relevant? Do we need to add new ones?
    3. (5 min) Round the table: Claim one task each.


* 9:15 Prepare agenda for teacher meeting

* 9:30 Teacher meetings

    All 1. Semester groups and one or more members from 3. Semester groups  

    Timeslot for your weekly 15 minute meeting with the teachers.  

    Remember to book a time and have an agenda prepared.  

    Add minutes of meeting in your gitlab project  

    | Time   | Group | Room  | Teacher |
    |:------ |:----- |:----- |:------- |
    | 9:30   | G1    | B2.19 | NISI    |
    | 9:30   | G2    | B2.05 | ILES   |
    | 9:45   | G3    | B2.19 | NISI    |
    | 9:45   | G4    | B2.05 | ILES   |
    | 10:00   | G5    | B2.05 | ILES   |

* 09:45 Survey strategy

    Groups make a strategy how to start preparing for:  
    1. Power point presentation on user study/survey  
    2. pre-product launch questionnaire 
 
    Note: each member of the group should have a specific task!

 * 10:45 Survey preperation

    The following should be taken into concideration when prepareing for PPP :

    1. What is a survey
    2. What is a user study and the different types and its importance and application
    3. How questionnaire should be done
    4. Which platforms and medium should be use for better feedback and why
    5. Why survey and user studies should be performed  before and after product is launched. 

 * 15:30- until next week

    Launching the survey and user studies

## Hands-on time

Survey and task work

## Comments

### Useful survey related links:

[getapp](https://www.getapp.com/)  
[google forms](https://www.google.com/forms/about/)

Remember to ask questions if you have any!

\pagebreak