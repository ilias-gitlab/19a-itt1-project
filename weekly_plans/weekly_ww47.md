---
Week: 47
Content:  Development phase
Material: See links in weekly plan
Initials: NISI/ILES
---

# Week 47 ITT1 Project - Consolidation

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* All groups presents survey results

### Learning goals

The student can:

* Analyze and present survey data

## Deliverables

1. Mandatory weekly meetings with the teachers (this includes minutes of the meeting)
    
    * Agenda is:
        1. Status on project (ie. show closed tasks in gitlab)
        2. Next steps (ie. show next tasks in gitlab)
        3. Collaboration within the group (ie. any internal issues, fairness of workload, communication)
        4. Help needed or offered (ie. what help do you need and where do you feel you can contribute to the class)
        5. Attendance
        6. Any other business (AOB)

2. Minutes of meeting, from weekly teacher meeting, included in appropiate folder in gitlab project

## Schedule Monday 2019-11-18

* 8:15 Introduction to the day, general Q/A session

    * Fablab workshops not possible
    * OLA17 on wiseflow later today

* 8:30 Jalal Azadi - Hannover Messe 2019

    * Jalal is lecturing at the Automation education and would like to offer you a trip to Hannover Messe in April 2020.  
    The trip is 3 days in total with two accomodations.  
    Info about the Messe [Hannovermesse.de](https://www.hannovermesse.de/home)

* 09:00 Group morning meetings

You will meet in your groups every morning and decide on who does what. This is part of the project management, and is mandatory.
    Ordinary agenda:
    1. (5 min) Round the table: What did I do, and what did I finish?
    2. (5-10 min) Review of tasks: Are they still relevant? Do we need to add new ones?
    3. (5 min) Round the table: Claim one task each.

* 9:15 Prepare agenda for Teacher meeting

    All 1. Semester groups and one or more members from 3. Semester groups  

    Timeslot for your weekly 15 minute meeting with the teachers.  

    Remember to book a time and have an agenda prepared.  

    Add minutes of meeting in your gitlab project  

    | Time   | Group | Room  | Teacher |
    |:------ |:----- |:----- |:------- |
    | 09:30   | G1    | B2.19 | ILES    |
    | 09:30   | G2    | B2.05 | NISI    |
    | 09:45  | G3    | B2.19 | ILES    |
    | 09:45  | G4    | B2.05 | NISI    |
    | 10:00  | G5    | B2.19 | ILES    |

* 09:30 Hands on time 

    * Do exercise 0
    * Do planned task work

* 12:15 Survey presentations in B2.05/B2.49

*  Each group presents their survey PPP - See Exercise 0 for details
    * Participation is mandatory
    * Presentation duration maximum 10 minutes for each group
    * Everyone in the group has to say something at the presentation, it might be a good idea to talk about the specific survey task(s) that you worked with.

* 13:30 Hands on time

    * Continue planned task work

* 15:30 End of day

### Exercise 0 - Prepare survey PPP  

* Prepare the survey PPP in order to be ready for the presentations after lunch.  
* Remember to include the survey results and PPP in your gitlab project

PPP preperation required minimum content:

1. Different types of surveys including pros and cons
2. Which platforms did you consider and why did you choose a specific one ?
3. Your survey - what was the goal of it and how did you perform it ?
4. Analyzed survey results summary - Please use diagrams and graphs when possible
5. How will the survey results affect your product development ? 




## Comments

\pagebreak