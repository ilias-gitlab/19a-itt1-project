---
Week: 48
Content:  Development phase
Material: See links in weekly plan
Initials: NISI/ILES
---

# Week 48 ITT1 Project - Prototype

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* All groups have planned milestones for the prototype phase
* All groupmembers are working on prototype tasks
* All groups participate in the status meeting   

### Learning goals

The student knows:

* What a prototype is

The student can:

* Implement research, development and consolidation into a prototype
* Plan for the prototype phase

## Deliverables

1. Mandatory weekly meetings with the teachers (this includes minutes of the meeting)
    
    * Agenda is:
        1. Status on project (ie. show closed tasks in gitlab)
        2. Next steps (ie. show next tasks in gitlab)
        3. Collaboration within the group (ie. any internal issues, fairness of workload, communication)
        4. Help needed or offered (ie. what help do you need and where do you feel you can contribute to the class)
        5. Attendance
        6. Any other business (AOB)

2. Minutes of meeting, from weekly teacher meeting, included in appropiate folder in gitlab project

## Schedule Monday 2019-11-25

* 8:15 Introduction to the day, general Q/A session

    * OLA17 questions ?
    * The prototype phase (3 weeks)
    * Prototype ? What is it ??
    * Hannover Messe 2020 reminder

* 9:00 Group morning meetings

    Please look at todays exercise before you do your planning!

    You will meet in your groups every morning and decide on who does what. This is part of the project management, and is mandatory.
    Ordinary agenda:
    1. (5 min) Round the table: What did I do, and what did I finish?
    2. (5-10 min) Review of tasks: Are they still relevant? Do we need to add new ones?
    3. (5 min) Round the table: Claim one task each.

* 9:15 Prepare agenda for teacher meeting

* 9:30 Teacher meetings

    All 1. Semester groups and one or more members from 3. Semester groups  

    Timeslot for your weekly 15 minute meeting with the teachers.  

    Remember to book a time and have an agenda prepared.  

    Add minutes of meeting in your gitlab project  

    | Time  | Group | Room  | Teacher |
    |:----- |:----- |:----- |:------- |
    | 9:30  | G1    | B2.19 | NISI    |
    | 9:45  | G2    | B2.19 | NISI    |
    | 9:30  | G3    | B2.05 | ILES    |
    | 9:45  | G4    | B2.05 | ILES    |
    | 10:00 | G5    | B2.19 | NISI    |


* 10:00 Hands on time

* 15:00 Joint status meeting

    To ensure that you are on track we will do status meeting at the end of the day for the rest of the 1st semester project.

    Each group gives a 5-minute status of the day following the below agenda:

    1. What tasks did you plan for today ?
    2. Did you manage to complete you planned tasks ?
    3. What is the milestone for next week ?

    The meeting is mandatory for all groups

## Hands-on time

### Exercise 0 - Prototype plan

The prototype phase of the project has a duration of 3 weeks (48, 49, 50) and the goal of this phase is to bring the work of the 3 previous project phases together in a prototype that can be presented at the Project presentations in week 2.

See the [lecture plan](https://eal-itt.gitlab.io/19a-itt1-project/19A_ITT1_project_lecture_plan.pdf) for more info on project phases.

In your group use [**Circle of knowledge** or **Round table**](https://eal-itt.gitlab.io/19a-itt1-programming/19A_ITT1_cooperative_learning_structures.pdf#section.2) to discuss/clarify:

1. What will be included in your prototype (think realistic according to what you already have!)
2. Is it going to be incorporated in 3rd semesters prototype?
2. How are you going to document the prototype phase in your gitlab project ?
3. How will you present/demo your prototype ?
4. What output do you need for each project week in the prototype phase (prototype phase milestones)
5. What tasks do we need to complete today ?

The exercise should take no more than 30 mins so remember to be efficient and focus on decisions!

Remember to translate your decisions as milestones and tasks on gotlab.  
At the end of the day there will be a joint meeting between all project groups where you tell the status of the day.

## Comments

* Read the [Prototype description](https://en.wikipedia.org/wiki/Prototype)

* We are closing in on the end of the 1st semester project. 

    To conclude the project each group will have to hand in a project report in week 51 (OLA13) and a presentation of the project in week 2 (OLA12)

    More information about this in week 50

\pagebreak