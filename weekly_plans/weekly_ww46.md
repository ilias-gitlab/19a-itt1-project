---
Week: 46
Content:  Development phase
Material: See links in weekly plan
Initials: NISI/ILES
---

# Week 46 ITT1 Project - Consolidation

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

All groups are: 
* Working on tasks
* Performing survey
* Analyzing survey results

### Learning goals

The student can:

* Launch a survey
* Analyze survey data

## Deliverables

1. Mandatory weekly meetings with the teachers (this includes minutes of the meeting)
    
    * Agenda is:
        1. Status on project (ie. show closed tasks in gitlab)
        2. Next steps (ie. show next tasks in gitlab)
        3. Collaboration within the group (ie. any internal issues, fairness of workload, communication)
        4. Help needed or offered (ie. what help do you need and where do you feel you can contribute to the class)
        5. Attendance
        6. Any other business (AOB)

2. Minutes of meeting, from weekly teacher meeting, included in appropiate folder in gitlab project

## Schedule Monday 2019-11-11

* 8:15 Introduction to the day, general Q/A session

* 8:45 Group morning meetings


You will meet in your groups every morning and decide on who does what. This is part of the project management, and is mandatory.
    Ordinary agenda:
    1. (5 min) Round the table: What did I do, and what did I finish?
    2. (5-10 min) Review of tasks: Are they still relevant? Do we need to add new ones?
    3. (5 min) Round the table: Claim one task each.

* 9:15 Prepare agenda for Teacher meeting

    All 1. Semester groups and one or more members from 3. Semester groups  

    Timeslot for your weekly 15 minute meeting with the teachers.  

    Remember to book a time and have an agenda prepared.  

    Add minutes of meeting in your gitlab project  

    | Time   | Group | Room  | Teacher |
    |:------ |:----- |:----- |:------- |
    | 9:30   | G1    | B2.19 | ILES    |
    | 9:45   | G2    | B2.19 | ILES    |
    | 10:00  | G3    | B2.19 | ILES    |
    | 10:00  | G4    | B2.05 | NISI    |
    | 10:15  | G5    | B2.05 | NISI    |

* 9:30 Hands on time 

### Launching the Survey

* Perform a survey on relevant platform or face to face at UCL campus.

### Analyzing Survey data

* Find trends in user feedback, ie. does a majority suggest a change ?
* Correct your product specifications according to analyzed data

### PPP preperation

Minimum content:

* Different types of surveys including pros and cons
* Which platforms did you consider and why did you choose a specific one ?
* Your survey - what was the goal of it and how did you perform it ?
* Analyzed survey results summary
* How will it affect your product development

Everyone in the group has to say something at the presentation, it might be a good idea to talk about the specific survey task(s) that you worked with.

## Hands-on time

Continue task work

## Comments

Remember to ask questions if you have any!
## useful links:
https://www.getapp.com/  
https://www.google.com/forms/about/


\pagebreak