---
Week: 50
Content:  Development phase
Material: See links in weekly plan
Initials: NISI/ILES
---

# Week 50 ITT1 Project - Prototype

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* All groupmembers are working on prototype tasks
* All groups participate in the report writting

### Learning goals

The student knows:

How to write a technical report

The student can:

* write a technical report

## Deliverables



## Schedule Monday 2019-12-09

* 8:15 Introduction to the day, general Q/A session

    
    * Report writing and requirements (OLA13)
    * Poster presentation (OLA 12)
    
* 9:00 Group morning meetings

    Please look at todays exercise before you do your planning!

    You will meet in your groups every morning and decide on who does what. This is part of the project management, and is mandatory.
    Ordinary agenda:
    1. (5 min) Round the table: What did I do, and what did I finish?
    2. (5-10 min) Review of tasks: Are they still relevant? Do we need to add new ones?
    3. (5 min) Round the table: Claim one task each.

* 9:30 Hands-on time

* 15:30 End of day


## Hands-on time

* Carry out the tasks you decided on for today.

## Comments

\pagebreak