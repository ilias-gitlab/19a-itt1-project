---
Week: 44
Content:  Development phase
Material: See links in weekly plan
Initials: NISI/ILES
---

# Week 44 ITT1 Project - Development

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* All groups are working on tasks   

### Learning goals

Students can do group management   
Students can do project management  
Students can define and work with s.m.a.r.t tasks  
Students have knowledge about ESD  

## Deliverables

1. Mandatory weekly meetings with the teachers (this includes minutes of the meeting)
    
    * Agenda is:
        1. Status on project (ie. show closed tasks in gitlab)
        2. Next steps (ie. show next tasks in gitlab)
        3. Collaboration within the group (ie. any internal issues, fairness of workload, communication)
        4. Help needed or offered (ie. what help do you need and where do you feel you can contribute to the class)
        5. Attendance
        6. Any other business (AOB)

2. Minutes of meeting, from weekly teacher meeting, included in appropiate folder in gitlab project

## Schedule Monday 2019-10-28

* 8:15 Introduction to the day, general Q/A session

* 8:45 Prepare agenda for teacher meeting

* 9:00 Teacher meetings

    All 1. Semester groups and one or more members from 3. Semester groups  

    Timeslot for your weekly 15 minute meeting with the teachers.  

    Remember to book a time and have an agenda prepared.  

    Add minutes of meeting in your gitlab project  

    | Time   | Group | Room  | Teacher |
    |:------ |:----- |:----- |:------- |
    | 9:00   | G1    | B2.19 | NISI    |
    | 9:15   | G2    | B2.19 | NISI    |
    | 9:30   | G3    | B2.05 | NISI    |
    | 9:45   | G4    | B2.05 | NISI    |
    | 10:00  | G5    | B2.19 | NISI    |

* 9:30 - 10:00 Group morning meetings

    You will meet in your groups every morning and decide on who does what. This is part of the project management, and is mandatory.
    Ordinary agenda:
    1. (5 min) Round the table: What did I do, and what did I finish?
    2. (5-10 min) Review of tasks: Are they still relevant? Do we need to add new ones?
    3. (5 min) Round the table: Claim one task each.

* 10:00 Hands on time

* 11:00 - 12:00 Group 4 SEF presentation with Digital Concept Design

* 13:00 ESD Course from HIN A/S (mandatory for all)

## Hands-on time

Continue task work

## Comments

Remember to ask questions if you have any!

\pagebreak