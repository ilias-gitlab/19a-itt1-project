---
Week: 40
Content:  Idea and research
Material: See links in weekly plan
Initials: NISI/ILES
---

# Week 40 ITT1 Project - Idea and research

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* All groups have an overview of the project they are part of
* All group members are working on an s.m.a.r.t task decided in the group morning meeting  

### Learning goals

* Visual thinking in a project context
* Block diagrams
* Milestones

## Deliverables

1. Mandatory weekly meetings with the teachers (this includes minutes of the meeting)
    * Extended to 30 minutes this week.
    
    * Agenda is:
        1. Status on project (ie. show closed tasks in gitlab)
        2. Next steps (ie. show next tasks in gitlab)
        3. Collaboration within the group (ie. any internal issues, fairness of workload, communication)
        4. Help needed or offered (ie. what help do you need and where do you feel you can contribute to the class)
        5. OLA requirements
        6. Attendance
        7. Any other business (AOB)

2. Minutes of meeting, from weekly teacher meeting, included in appropiate folder in gitlab project

3. OLA11 completed with hand-in on wiseflow
4. Graphical project overview in project plan
5. Project milestones decided and added in project plan + gitlab

## Schedule Monday 2019-09-30

* 8:15 Introduction to the day, general Q/A session

    Feedback session about project (15 minutes)

* 9:00 Visual thinking

    [Link to slides](https://docs.google.com/presentation/d/17jEPrx_qHF4jm4nVKerXkfIGN6kzl4EFPGAf9pRMB4E/edit?usp=sharing)

* 9:45 Prepare agenda for teacher meeting

* 10:00 Teacher meetings (in network lab)

    All 1. Semester groups and one or more members from 3. Semester groups  

    Timeslot for your weekly meeting with the teachers.  

    Remember to book a time and have an agenda prepared.  

    Add minutes of meeting in your gitlab project  

* 10:00 Group morning meeting

    You will meet in your groups every morning and decide on who does what. This is part of the project management, and is mandatory.
    Ordinary agenda:
    1. (5 min) Round the table: What did I do, and what did I finish?
    2. (5-10 min) Review of tasks: Are they still relevant? do we need to add new ones?
    3. (5 min) Round the table: Claim one task each.

* 10:30 Hands on time

## Hands-on time

* Exercise 0: Milestones and graphical project overview
    1. Create one or more milestones for each of the projects phases. 
    This will vary from project to project and can also change as the project evolves.  
    See the [lectureplan](https://eal-itt.gitlab.io/19a-itt1-project/19A_ITT1_project_lecture_plan.pdf) for an overview of the project phases common for all projects.  
    2. Create a graphical overview of your project milestones and include it in the project plan. 
    3. Create the same milestones in your gitlab project.  
    Read about milestones and how to create them in gitlab [here](https://docs.gitlab.com/ee/user/project/milestones/)  

* Exercise 1: Project work
    
    * Task work as agreed in the group morning meeting. 
    * OLA work (see wiseflow for details)

## Comments

Remember to ask questions if you have any!

\pagebreak